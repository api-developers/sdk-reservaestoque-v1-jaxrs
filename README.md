# SDK Java JAX-RS - Reserva Estoque V1

Durante a integração com o Marketplace da Cnova, há momentos onde existe a necessidade de comunicação ou consulta de informações específicas situadas na plataforma do lojista.

![Fluxo da API do Lojista](http://cnovaportaldev.sensedia.com/api-portal/sites/default/files/images/sdk_serv.png)

A estrutura de código aqui apresentada tem o objetivo de fornecer uma implementação básica para construção do recurso de consulta de reserva de estoque diretamente no lojista, podendo ser utilizada apenas como referência de implementação.

## Desenvolvimento

Os fontes desse projeto apenas expõe uma interface Restful no servidor onde o arquivo WAR for implantado.

Para integração com seu sistema, procure pela classe _com/cnova/seller/api/stockreserve/v1/impl/StockReserveApiServiceImpl.java_ e faça as implementações necessárias.

### Exemplo de contrução de retorno

O bloco abaixo representa uma estrutura de valores para a operação de reserva de estoque, de acordo com as classes de modelo presentes nessa implementação.

```java
public Response postStockReserve(String orderId,String skuIdList,String zipCode)
throws NotFoundException {

	StockReserveResponse srr = new StockReserveResponse();
	
	srr.setSellerMpToken("ncPJ8iS9A028JsnA92Hsnla21");
	
	SkuReserved sku1 = new SkuReserved();
	sku1.setSkuIdOrigin("28812");
	sku1.setQuantityReserved(Integer.valueOf(3));
	sku1.setIdStockReservation("100029");
	sku1.setErrorMessage("Não foi possível realizar a reserva de estoque.");
	
	srr.getSkuReserveds().add(sku1);
	
	SkuReserved sku2 = new SkuReserved();
	sku2.setSkuIdOrigin("28812");
	sku2.setQuantityReserved(Integer.valueOf(3));
	sku2.setIdStockReservation("100029");
	sku2.setErrorMessage("Não foi possível realizar a reserva de estoque.");
	
	srr.getSkuReserveds().add(sku2);
	
	return Response.ok().entity(srr).build();
		
}
```

Para operações que não devem retornar conteúdo no corpo da mensagem, basta retornar uma resposta com HTTP 200:

```java
return Response.ok().build();
```
