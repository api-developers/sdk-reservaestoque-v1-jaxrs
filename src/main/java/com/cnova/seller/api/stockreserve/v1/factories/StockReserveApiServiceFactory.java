package com.cnova.seller.api.stockreserve.v1.factories;

import com.cnova.seller.api.stockreserve.v1.StockReserveApiService;
import com.cnova.seller.api.stockreserve.v1.impl.StockReserveApiServiceImpl;

public class StockReserveApiServiceFactory {

    private final static StockReserveApiService service = new StockReserveApiServiceImpl();

    public static StockReserveApiService getStockReserveApi()
    {
        return service;
    }
}
