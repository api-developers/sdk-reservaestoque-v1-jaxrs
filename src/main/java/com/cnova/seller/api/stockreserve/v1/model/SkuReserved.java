package com.cnova.seller.api.stockreserve.v1.model;


import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class SkuReserved  {
  
  private String skuIdOrigin = null;
  private Integer quantityReserved = null;
  private String idStockReservation = null;
  private String errorMessage = null;

  
  /**
   * SKU ID do produto do lojista
   **/
  @ApiModelProperty(value = "SKU ID do produto do lojista")
  @JsonProperty("skuIdOrigin")
  public String getSkuIdOrigin() {
    return skuIdOrigin;
  }
  public void setSkuIdOrigin(String skuIdOrigin) {
    this.skuIdOrigin = skuIdOrigin;
  }

  
  /**
   * Quantidade reservada no lojista
   **/
  @ApiModelProperty(value = "Quantidade reservada no lojista")
  @JsonProperty("quantityReserved")
  public Integer getQuantityReserved() {
    return quantityReserved;
  }
  public void setQuantityReserved(Integer quantityReserved) {
    this.quantityReserved = quantityReserved;
  }

  
  /**
   * Identificador da reserva
   **/
  @ApiModelProperty(value = "Identificador da reserva")
  @JsonProperty("idStockReservation")
  public String getIdStockReservation() {
    return idStockReservation;
  }
  public void setIdStockReservation(String idStockReservation) {
    this.idStockReservation = idStockReservation;
  }

  
  /**
   * Mensagem de erro caso não ocorra a reserva
   **/
  @ApiModelProperty(value = "Mensagem de erro caso não ocorra a reserva")
  @JsonProperty("errorMessage")
  public String getErrorMessage() {
    return errorMessage;
  }
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class SkuReserved {\n");
    
    sb.append("  skuIdOrigin: ").append(skuIdOrigin).append("\n");
    sb.append("  quantityReserved: ").append(quantityReserved).append("\n");
    sb.append("  idStockReservation: ").append(idStockReservation).append("\n");
    sb.append("  errorMessage: ").append(errorMessage).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
