package com.cnova.seller.api.stockreserve.v1;

import com.cnova.seller.api.stockreserve.v1.*;
import com.cnova.seller.api.stockreserve.v1.model.*;

import com.sun.jersey.multipart.FormDataParam;

import com.cnova.seller.api.stockreserve.v1.model.StockReserveResponse;

import java.util.List;
import com.cnova.seller.api.stockreserve.v1.NotFoundException;

import java.io.InputStream;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.core.Response;

public abstract class StockReserveApiService {
  
    public abstract Response postStockReserve(String orderId,String skuIdList,String zipCode)
    throws NotFoundException;
  
    public abstract Response postStockReserveCancelation(String idStockReservation)
    throws NotFoundException;
  
    public abstract Response postStockReserveConfirmation(String idStockReservation)
    throws NotFoundException;
  
}
