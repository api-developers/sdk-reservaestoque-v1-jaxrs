package com.cnova.seller.api.stockreserve.v1.model;

import com.cnova.seller.api.stockreserve.v1.model.SkuReserved;
import java.util.*;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class StockReserveResponse  {
  
  private List<SkuReserved> skuReserveds = new ArrayList<SkuReserved>() ;
  private String sellerMpToken = null;

  
  /**
   * Lista de SKUs reservados
   **/
  @ApiModelProperty(value = "Lista de SKUs reservados")
  @JsonProperty("skuReserveds")
  public List<SkuReserved> getSkuReserveds() {
    return skuReserveds;
  }
  public void setSkuReserveds(List<SkuReserved> skuReserveds) {
    this.skuReserveds = skuReserveds;
  }

  
  /**
   * Token de identificação do lojista no parceiro. Para garantir origem da informação. Deve ser utilizado o mesmo Auth-Token das chamadas à API
   **/
  @ApiModelProperty(value = "Token de identificação do lojista no parceiro. Para garantir origem da informação. Deve ser utilizado o mesmo Auth-Token das chamadas à API")
  @JsonProperty("sellerMpToken")
  public String getSellerMpToken() {
    return sellerMpToken;
  }
  public void setSellerMpToken(String sellerMpToken) {
    this.sellerMpToken = sellerMpToken;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class StockReserveResponse {\n");
    
    sb.append("  skuReserveds: ").append(skuReserveds).append("\n");
    sb.append("  sellerMpToken: ").append(sellerMpToken).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
