package com.cnova.seller.api.stockreserve.v1;

import com.cnova.seller.api.stockreserve.v1.model.*;
import com.cnova.seller.api.stockreserve.v1.StockReserveApiService;
import com.cnova.seller.api.stockreserve.v1.factories.StockReserveApiServiceFactory;

import io.swagger.annotations.ApiParam;

import com.sun.jersey.multipart.FormDataParam;

import com.cnova.seller.api.stockreserve.v1.model.StockReserveResponse;

import java.util.List;
import com.cnova.seller.api.stockreserve.v1.NotFoundException;

import java.io.InputStream;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.core.Response;
import javax.ws.rs.*;

@Path("/stockReserve")
@Consumes({ "application/json" })

@io.swagger.annotations.Api(value = "/stockReserve", description = "the stockReserve API")
public class StockReserveApi  {

    private final StockReserveApiService delegate = StockReserveApiServiceFactory.getStockReserveApi();
    
    @POST
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "", notes = "Informar ao lojista que houve uma compra de determinado produto, para que ele possa controlar do lado do e-commerce dele a disponibilidade do produto.", response = StockReserveResponse.class)
    @io.swagger.annotations.ApiResponses(value = { 
    @io.swagger.annotations.ApiResponse(code = 200, message = "OK") })
    public Response postStockReserve(@ApiParam(value = "ID master do pedido, que é o ID passado para o cliente. Corresponde ao campo orderMasterId de um pedido.",required=true) @QueryParam("orderId") String orderId,
    @ApiParam(value = "Lista de SKU ID do produto do lojista e quantidade separados por vírgula.",required=true) @QueryParam("skuIdList") String skuIdList,
    @ApiParam(value = "CEP de entrega.",required=true) @QueryParam("zipCode") String zipCode)
    throws NotFoundException {
        return delegate.postStockReserve(orderId,skuIdList,zipCode);
    }
    
    @POST
    @Path("/cancelation/{idStockReservation}")
    
    
    @io.swagger.annotations.ApiOperation(value = "", notes = "Operação para confirmação da reserva de um produto que foi comprado pelo Marketplace.", response = Void.class)
    @io.swagger.annotations.ApiResponses(value = { 
    @io.swagger.annotations.ApiResponse(code = 200, message = "OK") })
    public Response postStockReserveCancelation(@ApiParam(value = "Identificação da reserva retornada pelo parceiro",required=true ) @PathParam("idStockReservation") String idStockReservation)
    throws NotFoundException {
        return delegate.postStockReserveCancelation(idStockReservation);
    }
    
    @POST
    @Path("/confirmation/{idStockReservation}")
    
    
    @io.swagger.annotations.ApiOperation(value = "", notes = "Operação para confirmação da reserva de um produto que foi comprado pelo Marketplace.", response = Void.class)
    @io.swagger.annotations.ApiResponses(value = { 
    @io.swagger.annotations.ApiResponse(code = 200, message = "OK") })
    public Response postStockReserveConfirmation(@ApiParam(value = "Identificação da reserva retornada pelo parceiro",required=true ) @PathParam("idStockReservation") String idStockReservation)
    throws NotFoundException {
        return delegate.postStockReserveConfirmation(idStockReservation);
    }
}


